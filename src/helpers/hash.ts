export const generateCodeChallenge = async (codeVerifier: string) => {
  var digest = await crypto.subtle.digest(
    "SHA-256",
    new TextEncoder().encode(codeVerifier)
  );

  return btoa(String.fromCharCode(...new Uint8Array(digest)))
    .replace(/=/g, "")
    .replace(/\+/g, "-")
    .replace(/\//g, "_");
};

async function sha256(text: string) {
  const encoder = new TextEncoder();
  const data = encoder.encode(text);
  const digest = await window.crypto.subtle.digest("SHA-256", data);
  const sha = String.fromCharCode(...new Uint8Array(digest));
  return sha;
}

export async function createCodeChallenge(codeVerifier: string) {
  const sha = await sha256(codeVerifier);
  // https://tools.ietf.org/html/rfc7636#appendix-A
  return btoa(sha).split("=")[0].replace(/\+/g, "-").replace(/\//g, "_");
}
