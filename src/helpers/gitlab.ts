import { LoaderFunction, redirect } from "react-router-dom";
import { Counter } from "../model/counter-response";
import { GitlabTokenResponse } from "../model/gitlab-token-response";
import { State } from "../model/state";
import { createCodeChallenge, generateCodeChallenge } from "./hash";

const CLIENT_ID = import.meta.env.VITE_GITLAB_CLIENT_ID;
const REDIRECT_URI = import.meta.env.VITE_REDIRECT_URI;
const SCOPE = "read_user write_repository api";
const SNIPPET_ID = import.meta.env.VITE_GITLAB_SNIPPET_ID;

export const authorizeWithGitlab = async () => {
  const state = crypto.randomUUID();
  const codeVerifier = crypto.randomUUID() + "-" + crypto.randomUUID();
  const codeChallenge = await createCodeChallenge(codeVerifier);

  const params = new URLSearchParams({
    client_id: CLIENT_ID,
    redirect_uri: REDIRECT_URI,
    response_type: "code",
    scope: SCOPE,
    state,
    code_challenge: codeChallenge,
    code_challenge_method: "S256",
  });

  window.sessionStorage.setItem(
    "state",
    JSON.stringify({ state, verifier: codeVerifier } as State)
  );

  const url = `https://gitlab.com/oauth/authorize?` + params.toString();

  window.location.href = url;
};

export const getGitlabToken = async (code: string, verifier: string) => {
  const params = new URLSearchParams({
    client_id: CLIENT_ID,
    redirect_uri: REDIRECT_URI,
    grant_type: "authorization_code",
    code_verifier: verifier,
    code,
  });

  const url = "https://gitlab.com/oauth/token?" + params.toString();
  const res = await fetch(url, {
    method: "POST",
  });

  if (!res.ok) {
    throw new Error(await res.json());
  }

  const tokenResponse: GitlabTokenResponse = await res.json();

  window.localStorage.setItem("token", JSON.stringify(tokenResponse));
};

export const handleRedirect: LoaderFunction = async ({ request }) => {
  const params = new URLSearchParams(request.url.split("?")[1]);

  const code = params.get("code");
  const state = params.get("state");

  if (!params || !code || !state) {
    throw new Response("Invalid url", { status: 400 });
  }

  const stateJson = sessionStorage.getItem("state");
  if (!stateJson) {
    throw new Response("Invalid state", { status: 400 });
  }

  const loadedState = JSON.parse(stateJson) as State;

  if (loadedState.state !== state) {
    throw new Response("Invalid state", { status: 400 });
  }

  sessionStorage.removeItem("state");

  await getGitlabToken(code, loadedState.verifier);

  return redirect(import.meta.env.BASE_URL);
};

export const getCountValue = async (access_token: string) => {
  const res = await fetch(
    `https://gitlab.com/api/v4/snippets/${SNIPPET_ID}/raw`,
    {
      headers: {
        Authorization: `Bearer ${access_token}`,
      },
    }
  );

  if (!res.ok) {
    throw new Error("Error reading counter value");
  }

  const text = await res.text();

  return JSON.parse(text) as Counter;
};

export const setCountValue = async (
  access_token: string,
  newValue: Counter
) => {
  const res = await fetch(`https://gitlab.com/api/v4/snippets/${SNIPPET_ID}`, {
    method: "PUT",
    headers: {
      Authorization: `Bearer ${access_token}`,
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      files: [
        {
          action: "update",
          file_path: "counter.json",
          content: JSON.stringify(newValue),
        },
      ],
    }),
  });

  if (!res.ok) {
    throw new Error("Error updating counter value");
  }

  return await res.json();
};

export const revoke = async (token: GitlabTokenResponse) => {
  const params = new URLSearchParams({
    client_id: CLIENT_ID,
    token: token.refresh_token,
  });

  const url = "https://gitlab.com/oauth/revoke?" + params.toString();
  await fetch(url, {
    method: "POST",
  });
};
