import { GitlabTokenResponse } from "../model/gitlab-token-response";

export const USER_KEY = "user";
export const TOKEN_KEY = "token";

export const loadFromLocalStorage = (): GitlabTokenResponse | undefined => {
  const accessTokenJson = localStorage.getItem(TOKEN_KEY);
  if (!accessTokenJson) {
    return undefined;
  }

  const token = JSON.parse(accessTokenJson) as GitlabTokenResponse;

  if (!token) {
    localStorage.removeItem(TOKEN_KEY);
    return undefined;
  }

  const expires_at = (token.created_at + token.expires_in) * 1000;

  if (expires_at <= Date.now() || !token) {
    localStorage.removeItem(TOKEN_KEY);
    return undefined;
  }

  return token;
};

export const clearUserLocalstorage = () => {
  localStorage.removeItem(TOKEN_KEY);
};
