/// <reference types="vite/client" />

interface ImportMetaEnv {
  readonly VITE_GOOGLE_API_CLIENT_ID: string;
  readonly VITE_SPREADSHEET_ID: string;
  readonly VITE_GITLAB_CLIENT_ID: string;
  readonly VITE_REDIRECT_URI: string;
  readonly VITE_GITLAB_SNIPPET_ID: string;
}

interface ImportMeta {
  readonly env: ImportMetaEnv;
}
