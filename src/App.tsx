import AddIcon from "@mui/icons-material/Add";
import LogoutIcon from "@mui/icons-material/Logout";
import RemoveIcon from "@mui/icons-material/Remove";
import LoadingButton from "@mui/lab/LoadingButton";
import {
  AppBar,
  Box,
  Button,
  Container,
  IconButton,
  Stack,
  TextField,
  Toolbar,
  Typography,
} from "@mui/material";
import { useCallback, useEffect, useState } from "react";
import "./App.css";
import {
  authorizeWithGitlab,
  getCountValue,
  revoke,
  setCountValue,
} from "./helpers/gitlab";
import { loadFromLocalStorage } from "./helpers/localstorage";
import { GitlabTokenResponse } from "./model/gitlab-token-response";

function App() {
  const [token, setToken] = useState<GitlabTokenResponse | undefined>(
    undefined
  );
  const [value, setValue] = useState<number>(0);
  const [isLoading, setIsLoading] = useState(false);

  const login = () => {
    authorizeWithGitlab();
  };

  const handleLogout = useCallback(() => {
    if (token) {
      revoke(token).then(() => {
        localStorage.clear();
        setToken(undefined);
      });
    }
  }, [token]);

  const handleSave = async () => {
    if (!token) {
      return;
    }

    setIsLoading(true);
    await setCountValue(token.access_token, { value });
    setIsLoading(false);
  };

  useEffect(() => {
    const token = loadFromLocalStorage();
    setToken(token);
  }, []);

  useEffect(() => {
    if (token) {
      setIsLoading(true);
      getCountValue(token.access_token).then((val) => {
        setValue(val.value);
        setIsLoading(false);
      });
    }
  }, [token]);

  return (
    <>
      <AppBar position="relative">
        <Toolbar>
          <Box
            sx={{
              borderRadius: "100%",
              bgcolor: "white",
              marginRight: "1rem",
              padding: "4x",
            }}
          >
            <img src="./abk-logo-black.svg" width={32} />
          </Box>
          <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
            Ølgæld
          </Typography>
          {token && (
            <Button
              color="inherit"
              onClick={handleLogout}
              endIcon={<LogoutIcon />}
            >
              Log ud
            </Button>
          )}
        </Toolbar>
      </AppBar>
      <Container maxWidth="sm" sx={{ marginTop: 4 }}>
        {!token ? (
          <Button variant="contained" fullWidth onClick={() => login()}>
            Log ind med GitLab
          </Button>
        ) : (
          <Stack direction={"column"} gap={4}>
            <Stack
              direction="row"
              gap={2}
              justifyContent="center"
              alignItems={"center"}
            >
              <IconButton
                onClick={() => setValue(Math.max(value - 1, 0))}
                disabled={isLoading}
              >
                <RemoveIcon />
              </IconButton>
              <TextField
                value={value}
                inputProps={{ inputMode: "numeric", pattern: "[0-9]*" }}
                disabled={isLoading}
                onChange={(e) =>
                  setValue(
                    e.target.value ? Number.parseInt(e.target.value, 10) : 0
                  )
                }
              />
              <IconButton
                onClick={() => setValue(value + 1)}
                disabled={isLoading}
              >
                <AddIcon />
              </IconButton>
            </Stack>
            <LoadingButton
              fullWidth
              variant="contained"
              onClick={handleSave}
              loading={isLoading}
            >
              Gem
            </LoadingButton>
          </Stack>
        )}
      </Container>
    </>
  );
}

export default App;
