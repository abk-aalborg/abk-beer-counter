export interface AccessToken {
  token: string;
  expires_at: number;
}
