import React from "react";
import ReactDOM from "react-dom/client";
import {
  createBrowserRouter,
  createRoutesFromElements,
  Route,
  RouterProvider,
  useRouteError,
} from "react-router-dom";
import App from "./App";
import "./index.css";

import { handleRedirect } from "./helpers/gitlab";

const router = createBrowserRouter(
  createRoutesFromElements(
    <>
      <Route path="/" element={<App />} />
      <Route
        path="/redirect"
        loader={(args) => handleRedirect(args)}
        element={<div>Redirecting...</div>}
        errorElement={<ErrorBoundary />}
      />
    </>
  ),
  { basename: import.meta.env.BASE_URL }
);

ReactDOM.createRoot(document.getElementById("root") as HTMLElement).render(
  <React.StrictMode>
    <RouterProvider router={router} />
  </React.StrictMode>
);

function ErrorBoundary() {
  let error = useRouteError();
  console.error(error);
  // Uncaught ReferenceError: path is not defined
  return <div>An error happened! See the console for more information.</div>;
}
